import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:notification/screen/test_page_two.dart';

class LocalNotificationService {
  static final FlutterLocalNotificationsPlugin _notificationsPlugin =
      FlutterLocalNotificationsPlugin();

  static void initialize(BuildContext context) {
    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: AndroidInitializationSettings("@mipmap/ic_launcher"));
    _notificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (payLoadMessage) async {
      Navigator.pushAndRemoveUntil(context,
          MaterialPageRoute(builder: (context) {
        return TestPageTwo(message: payLoadMessage);
      }), (route) => false);
    });
  }

  static void display(RemoteMessage message) {
    final id = DateTime.now().millisecondsSinceEpoch ~/ 1000;
    final NotificationDetails notificationDetails = NotificationDetails(
        android: AndroidNotificationDetails(
            "com.pondit.notification", "Notification Chanell", "My channel",
            importance: Importance.max, priority: Priority.high));
    _notificationsPlugin.show(id, message.notification.title,
        message.notification.body, notificationDetails,
        payload: message.data["message"]);
  }
}
