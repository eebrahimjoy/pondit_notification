import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:notification/localnotification/local_notification.dart';
import 'package:notification/screen/test_page_one.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    LocalNotificationService.initialize(context);
    //destroy app
    FirebaseMessaging.instance.getInitialMessage().then((message) {
      final messageFromFirebase = message.data["message"];

      Navigator.pushAndRemoveUntil(context,
          MaterialPageRoute(builder: (context) {
        return TestPageOne(message: messageFromFirebase);
      }), (route) => false);
    });

    //foreground
    FirebaseMessaging.onMessage.listen((mNotification) {
      if (mNotification.notification != null) {
        print("1111 /" + mNotification.notification.body);
        print("2222 /" + mNotification.notification.title);
      }

      LocalNotificationService.display(mNotification);
    });

    //background but click the notifaction
    FirebaseMessaging.onMessageOpenedApp.listen((mNotification) {
      final messageFromFirebase = mNotification.data["message"];

      Navigator.pushAndRemoveUntil(context,
          MaterialPageRoute(builder: (context) {
        return TestPageOne(message: messageFromFirebase);
      }), (route) => false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(18),
        child: Center(
          child: Text(
            'Will receive notification soon',
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
    );
  }
}
